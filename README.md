# stboot UEFI shim

This repo is for tracking Glasklar's stboot UEFI shim.  The review
submission is based on the [ST Shim Review](https://git.glasklar.is/system-transparency/project/st-shim-review)
project.
